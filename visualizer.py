from constants import ALL_SHOTS_DATA_CSV_FILENAME
from constants import ANGLE_DISTANCE_VISUALIZATION_FILENAME
from constants import PROBABILITY_ANGLE_VISUALIZATION_FILENAME
from constants import PROBABILITY_DEFENDER_DISTANCE_VISUALIZATION_FILENAME
from constants import PROBABILITY_DISTANCE_VISUALIZATION_FILENAME
from constants import XY_COORDINATES_VISUALIZATION_FILENAME
from typing import List
from sklearn.linear_model import LinearRegression

import copy
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import textwrap


def angleDistanceViz(shotsData: pd.DataFrame) -> None:
    fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2)

    madeShots = shotsData[shotsData["shot_made_flag"] == 1]
    unmadeShots = shotsData[shotsData["shot_made_flag"] == 0]

    ax1.scatter(
        madeShots["angle"],
        madeShots["shot_distance"],
        color=(0, 0.4, 0, 0.15),
    )
    ax1.set_xlabel("Angle")
    ax1.set_xticks(range(0, 91, 30))
    ax1.set_xlim(0, 90)
    ax1.set_ylabel("Distance (ft)")
    ax1.set_yticks(range(0, 91, 30))
    ax1.set_ylim(0, 90)

    ax2.scatter(
        unmadeShots["angle"],
        unmadeShots["shot_distance"],
        color=(1, 0, 0, 0.15),
    )
    ax2.set_xlabel("Angle")
    ax2.set_xticks(range(0, 91, 30))
    ax2.set_xlim(0, 90)
    ax2.set_ylabel("Distance (ft)")
    ax2.set_yticks(range(0, 91, 30))
    ax2.set_ylim(0, 90)

    fig.tight_layout()
    plt.show()
    plt.savefig(ANGLE_DISTANCE_VISUALIZATION_FILENAME)


def xyCoordinatesViz(shotsData: pd.DataFrame) -> None:
    fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2)
    fig.gca().set_aspect("equal", adjustable="box")

    madeShots = shotsData[shotsData["shot_made_flag"] == 1]
    unmadeShots = shotsData[shotsData["shot_made_flag"] == 0]

    ax1.scatter(madeShots["x"], madeShots["y"], color=(0, 0.4, 0, 0.15))
    ax1.scatter(0, 0, color=(0, 0, 0), s=10)
    ax1.set_xticks(range(-300, 301, 100))
    ax1.set_xlim(-300, 300)
    ax1.set_yticks(range(0, 1001, 200))
    ax1.set_ylim(-100, 1000)

    ax2.scatter(unmadeShots["x"], unmadeShots["y"], color=(1, 0, 0, 0.15))
    ax2.scatter(0, 0, color=(0, 0, 0), s=10)
    ax2.set_xticks(range(-300, 301, 100))
    ax2.set_xlim(-300, 300)
    ax2.set_yticks(range(0, 1001, 200))
    ax2.set_ylim(-100, 1000)

    fig.tight_layout()
    plt.show()
    plt.savefig(XY_COORDINATES_VISUALIZATION_FILENAME)


def linearRegression(x: np.array, y: np.array) -> List[float]:
    x = copy.deepcopy(x).reshape((-1, 1))
    linearRegressionModel = LinearRegression().fit(x, y)

    m = linearRegressionModel.coef_
    b = linearRegressionModel.intercept_
    rSquared = linearRegressionModel.score(x, y).round(5)

    return rSquared, m, b


def distanceProbabilityViz(shotsData: pd.DataFrame) -> None:
    fig, ax = plt.subplots()

    madeShots = shotsData[shotsData["shot_made_flag"] == 1]
    unmadeShots = shotsData[shotsData["shot_made_flag"] == 0]

    distanceTolerance = 0.51

    shotDistances = []
    probabilities = []

    for distance in np.arange(0, 91, 1):
        madeShotsApproximatelyFromDistance = madeShots[
            np.abs(madeShots["shot_distance"] - distance) < distanceTolerance
        ]
        unmadeShotsApproximatelyFromDistance = unmadeShots[
            np.abs(unmadeShots["shot_distance"] - distance) < distanceTolerance
        ]
        try:
            probability = len(madeShotsApproximatelyFromDistance) / (
                len(madeShotsApproximatelyFromDistance)
                + len(unmadeShotsApproximatelyFromDistance)
            )
        except ZeroDivisionError:
            probability = 0
        shotDistances.append(distance)
        probabilities.append(probability)

    shotDistances = np.array(shotDistances)
    probabilities = np.array(probabilities)

    ax.scatter(shotDistances, probabilities, color="black")

    rSquared, m, b = linearRegression(shotDistances, probabilities)

    plotTitle = textwrap.fill(
        f"Probability of scoring a shot from a given distance (R² = {rSquared})",
        25,
    )

    x = np.arange(0, 91, 30)
    y = m * x + b
    ax.plot(x, y, color="red")

    ax.set_xticks(range(0, 91, 30))
    ax.set_xlim(0, 90)
    ax.set_xlabel("Shot Distance (ft)")
    ax.set_yticks(np.arange(0, 1.1, 0.2))
    ax.set_ylim(0, 1)
    ax.set_ylabel("Probability")

    ax.set_title(plotTitle)

    fig.tight_layout()
    plt.show()
    plt.savefig(PROBABILITY_DISTANCE_VISUALIZATION_FILENAME)


def angleProbabilityViz(shotsData: pd.DataFrame) -> None:
    fig, ax = plt.subplots()
    madeShots = shotsData[shotsData["shot_made_flag"] == 1]
    unmadeShots = shotsData[shotsData["shot_made_flag"] == 0]

    angleTolerance = 0.51

    angles = []
    probabilities = []

    for angleDegree in range(0, 91, 1):
        madeShotsApproximatelyFromAngle = madeShots[
            np.abs(madeShots["angle"] - angleDegree) < angleTolerance
        ]
        unmadeShotsApproximatelyFromAngle = unmadeShots[
            np.abs(unmadeShots["angle"] - angleDegree) < angleTolerance
        ]
        try:
            probability = len(madeShotsApproximatelyFromAngle) / (
                len(madeShotsApproximatelyFromAngle)
                + len(unmadeShotsApproximatelyFromAngle)
            )
        except ZeroDivisionError:
            probability = 0
        angles.append(angleDegree)
        probabilities.append(probability)

    angles = np.array(angles)
    probabilities = np.array(probabilities)

    ax.scatter(angles, probabilities, color="black")

    rSquared, m, b = linearRegression(angles, probabilities)

    plotTitle = textwrap.fill(
        f"Probability of scoring a shot from a given angle (R² = {rSquared})",
        25,
    )

    x = np.arange(0, 1001, 200)
    y = m * x + b
    ax.plot(x, y, color="red")

    ax.set_xticks(range(0, 91, 30))
    ax.set_xlim(0, 90)
    ax.set_ylabel("Angle (degrees)")
    ax.set_yticks(np.arange(0, 1.1, 0.2))
    ax.set_ylim(0, 1)
    ax.set_ylabel("Probability")

    ax.set_title(plotTitle)

    fig.tight_layout()
    plt.show()
    plt.savefig(PROBABILITY_ANGLE_VISUALIZATION_FILENAME)


def defenderDistanceProbabilityViz(shotsData: pd.DataFrame) -> None:
    fig, ax = plt.subplots()

    madeShots = shotsData[shotsData["shot_made_flag"] == 1]
    unmadeShots = shotsData[shotsData["shot_made_flag"] == 0]

    distanceTolerance = 0.51

    defenderDistances = []
    probabilities = []

    for distance in np.arange(0, 73, 1):
        madeShotsApproximatelyFromDistance = madeShots[
            np.abs(madeShots["defender_distance"] - distance) < distanceTolerance
        ]
        unmadeShotsApproximatelyFromDistance = unmadeShots[
            np.abs(unmadeShots["defender_distance"] - distance) < distanceTolerance
        ]
        try:
            probability = len(madeShotsApproximatelyFromDistance) / (
                len(madeShotsApproximatelyFromDistance)
                + len(unmadeShotsApproximatelyFromDistance)
            )
        except ZeroDivisionError:
            probability = 0
        defenderDistances.append(distance)
        probabilities.append(probability)

    defenderDistances = np.array(defenderDistances)
    probabilities = np.array(probabilities)

    ax.scatter(defenderDistances, probabilities, color="black")

    rSquared, m, b = linearRegression(defenderDistances, probabilities)

    plotTitle = textwrap.fill(
        f"Probability of scoring a shot based on defender distance (R² = {rSquared})",
        25,
    )

    x = np.arange(0, 91, 30)
    y = m * x + b
    ax.plot(x, y, color="red")

    ax.set_xticks(range(0, 76, 15))
    ax.set_xlim(0, 75)
    ax.set_xlabel("Defender Distance (ft)")
    ax.set_yticks(np.arange(0, 1.1, 0.2))
    ax.set_ylim(0, 1)
    ax.set_ylabel("Probability")

    ax.set_title(plotTitle)

    fig.tight_layout()
    plt.show()
    plt.savefig(PROBABILITY_DEFENDER_DISTANCE_VISUALIZATION_FILENAME)


def main():
    print("Starting...")
    shotsData = pd.read_csv(ALL_SHOTS_DATA_CSV_FILENAME)
    print("Finished reading data...")
    angleDistanceViz(shotsData)
    print("Finished angleDistanceViz()...")
    xyCoordinatesViz(shotsData)
    print("Finished xyCoordinatesViz()...")
    distanceProbabilityViz(shotsData)
    print("Finished distanceProbabilityViz()...")
    angleProbabilityViz(shotsData)
    print("Finished angleProbabilityViz()...")
    defenderDistanceProbabilityViz(shotsData)
    print("Finished defenderDistanceProbabilityViz()...")
    print("Done.")


if __name__ == "__main__":
    main()
