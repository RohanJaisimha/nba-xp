NBA_SAVANT_URL_TEMPLATE = "https://www.nbasavant.com/csv.php?hfST=&hfQ=&hfSZB=&hfSZA=&hfSZR=&ddlYear={}&txtGameDateGT=&txtGameDateLT=&ddlGameTimeGT_min=&ddlGameTimeGT_sec=&ddlGameTimeLT_min=&ddlGameTimeLT_sec=&ddlShotDistanceGT=&ddlShotDistanceLT=&ddlTeamShooting={}&ddlTeamDefense={}&hfPT=&ddlGroupBy=team&ddlOrderBy=shots_made_desc&hfGT=0%7C1%7C&ddlShotMade=&ddlMin=0#results"

YEARS = [2014, 2015, 2016]
TEAMS = {
    "Atlanta Hawks": 1610612737,
    "Boston Celtics": 1610612738,
    "Cleveland Cavaliers": 1610612739,
    "New Orleans Pelicans": 1610612740,
    "Chicago Bulls": 1610612741,
    "Dallas Mavericks": 1610612742,
    "Denver Nuggets": 1610612743,
    "Golden State Warriors": 1610612744,
    "Houston Rockets": 1610612745,
    "Los Angeles Clippers": 1610612746,
    "Los Angeles Lakers": 1610612747,
    "Miami Heat": 1610612748,
    "Milwaukee Bucks": 1610612749,
    "Minnesota Timberwolves": 1610612750,
    "Brooklyn Nets": 1610612751,
    "New York Knicks": 1610612752,
    "Orlando Magic": 1610612753,
    "Indiana Pacers": 1610612754,
    "Philadelphia 76ers": 1610612755,
    "Phoenix Suns": 1610612756,
    "Portland Trail Blazers": 1610612757,
    "Sacramento Kings": 1610612758,
    "San Antonio Spurs": 1610612759,
    "Oklahoma City Thunder": 1610612760,
    "Toronto Raptors": 1610612761,
    "Utah Jazz": 1610612762,
    "Memphis Grizzlies": 1610612763,
    "Washington Wizards": 1610612764,
    "Detroit Pistons": 1610612765,
    "Charlotte Hornets": 1610612766,
}

ALL_SHOTS_CSV_UNCLEANED_FILENAME = "./DataFiles/AllShotsUncleaned.csv"
ALL_SHOTS_DATA_CSV_FILENAME = "./DataFiles/AllShotsData.csv"

TEMP_CSVS_WILDCARD = "./DataFiles/temp_dataFile_*.csv"

ANGLE_DISTANCE_VISUALIZATION_FILENAME = "./ImageFiles/AngleDistanceVisualization.png"
XY_COORDINATES_VISUALIZATION_FILENAME = "./ImageFiles/XYCoordinatesVisualization.png"
PROBABILITY_DISTANCE_VISUALIZATION_FILENAME = (
    "./ImageFiles/ProbabilityDistanceVisualization.png"
)
PROBABILITY_ANGLE_VISUALIZATION_FILENAME = (
    "./ImageFiles/ProbabilityAngleVisualization.png"
)
PROBABILITY_DEFENDER_DISTANCE_VISUALIZATION_FILENAME = (
    "./ImageFiles/ProbabilityDefenderDistanceVisualization.png"
)
NET = [0, 0]
