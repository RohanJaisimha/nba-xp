from constants import ALL_SHOTS_DATA_CSV_FILENAME
from constants import ALL_SHOTS_CSV_UNCLEANED_FILENAME

import pandas as pd


def readData() -> None:
    data = pd.read_csv(ALL_SHOTS_CSV_UNCLEANED_FILENAME)
    return data


def removeExtraneousColumns(data: pd.DataFrame) -> pd.DataFrame:
    data = data[["shot_made_flag", "x", "y", "shot_distance", "defender_distance"]]
    return data


def saveData(data: pd.DataFrame) -> None:
    data.to_csv(ALL_SHOTS_DATA_CSV_FILENAME, index=False)


def main():
    data = readData()
    data = removeExtraneousColumns(data)
    saveData(data)


if __name__ == "__main__":
    main()
