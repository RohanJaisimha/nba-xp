from constants import ALL_SHOTS_CSV_UNCLEANED_FILENAME
from constants import NBA_SAVANT_URL_TEMPLATE
from constants import TEAMS
from constants import TEMP_CSVS_WILDCARD
from constants import YEARS
from itertools import product

import glob
import os
import requests


def downloadCSV(url: str, count: int) -> None:
    fout = open(f"./DataFiles/temp_dataFile_{count}.csv", "wb")
    fout.write(requests.get(url).content)
    fout.close()


def deleteAllTempCSVs() -> None:
    tempCSVs = glob.glob(TEMP_CSVS_WILDCARD)
    for tempCSV in tempCSVs:
        os.remove(tempCSV)


def mergeCSVs() -> None:
    csvs = glob.glob(TEMP_CSVS_WILDCARD)
    fout = open(ALL_SHOTS_CSV_UNCLEANED_FILENAME, "w")

    skipOverallFirstLine = True

    for csv in csvs:
        fin = open(csv, "r")
        skipLine = True
        for line in fin:
            if skipLine and not skipOverallFirstLine:
                skipLine = False
                continue
            line = line.strip()
            fout.write(line + "\n")
            skipOverallFirstLine = False
        fin.close()

    fout.close()


def main():
    global NBA_SAVANT_URL_TEMPLATE

    count = 1
    for year, team1, team2 in product(YEARS, TEAMS.keys(), TEAMS.keys()):
        if team1 == team2:
            continue
        url = NBA_SAVANT_URL_TEMPLATE.format(year, TEAMS[team1], TEAMS[team2])
        downloadCSV(url, count)
        print(count)
        count += 1

    mergeCSVs()
    deleteAllTempCSVs()


if __name__ == "__main__":
    main()
