from constants import ALL_SHOTS_DATA_CSV_FILENAME
from constants import NET

import numpy as np
import pandas as pd


def calculateAngle(x: float, y: float) -> float:
    return (np.pi / 2 - abs(np.arctan(y / x))) * 180 / np.pi


def main():
    angles = []

    shotsData = pd.read_csv(ALL_SHOTS_DATA_CSV_FILENAME)

    for i in range(len(shotsData)):
        shot = shotsData.iloc[i]

        angles.append(calculateAngle(shot["x"], shot["y"]))

    shotsData["angle"] = angles

    shotsData.to_csv(ALL_SHOTS_DATA_CSV_FILENAME, index=False)


if __name__ == "__main__":
    main()
